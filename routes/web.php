<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/mahasiswa','MahasiswaController@index');




Route::get('/mahasiswa', [App\Http\Controllers\MahasiswaController::class,'reg_mahasiswa']);

Route::post('/simpan',[\App\Http\Controllers\MahasiswaController::class,'save']);
Route::get('/hapus/{id}',[\App\Http\Controllers\MahasiswaController::class,'delete']);

Route::get('/edit/{id}',[\App\Http\Controllers\MahasiswaController::class,'edit']);
Route::post('/update',[\App\Http\Controllers\MahasiswaController::class,'update']);
