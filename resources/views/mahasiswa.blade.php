@extends('layout.main')

@section('title', 'Tabel Mahasiswa')
@section('content')
    <section>
            <div class="conten">
                <h2>Mahasiswa</h2>
                
                <div class="newdata">
                    <h3>Tambah Data</h3>
                    <form action="/simpan" method="POST">
                        {{csrf_field()}}
                        <table class="tablenewdata">
                            <tr>
                                <td>Nama</td>
                                <td> <input type="text" name="nama" required="required" placeholder="Nama Lengkap" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>NIM</td>
                                <td> <input type="text" name="nim"  required="required"placeholder="NIM" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td> <input type="text" name="email"  required="required"placeholder="Email" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>No Telp</td>
                                <td> <input type="text" name="no_telp"  required="required" placeholder="No. Telp " class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>Prodi</td>
                                <td> <input type="text" name="prodi"  required="required" placeholder="Prodi" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>Jurusan</td>
                                <td><input type="text" name="jurusan" required="required" placeholder="Jurusan" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Fakultas</td>
                                <td> <input type="text" name="fakultas" required="required" placeholder="Falkultas" class="form-control">
                                </td>
                            </tr>
                        </table>
                        <input type="submit" value="DAFTAR" class="save">
                    </form>
                </div>

                <div class="studentsdata">
                    <table class="tablestudentsdata">
                        <tr class="segmen">
                            <td>NAMA</td>
                            <td>NIM</td>
                            <td>EMAIL</td>
                            <td>NO TELP</td>
                            <td>PRODI</td>
                            <td>JURUSAN</td>
                            <td>FAKULTAS</td>
                            <td>OPTION</td>
                        </tr>
                        @foreach ($data_mhs as $data)
                            <tr>
                                <td>{{$data->nama}}</td>
                                <td>{{$data->nim}}</td>
                                <td>{{$data->email}}</td>
                                <td>{{$data->no_telp}}</td>
                                <td>{{$data->prodi}}</td>
                                <td>{{$data->jurusan}}</td>
                                <td>{{$data->fakultas}}</td>
                                <td>
                                    <a href="/edit/{{$data->id}}" class="btn btn-warning text-white">EDIT</a>
                                    <a href="/hapus/{{$data->id}}"  class="btn btn-danger text-white" >HAPUS</a>                              
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>
    </section>
@endsection
