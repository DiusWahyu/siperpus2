@extends('layout.main')

@section('title', 'tabel Mahasiswa')
@section('content')
<section>
    <div class="conten">
        <h2>Tabel Mahasiswa</h2>
        <div class="newdata">
            <h3>Edit</h3>
            @foreach ($data_mhs as $data)
            <form action="/update" method="POST">
                {{csrf_field()}}
                <table class="tablenewdata">
                    <input type="hidden" name="id" value="{{ $data->id }}"> <br />
                    <tr>
                        <td>Nama</td>
                        <td> <input type="text" name="nama" value="{{ $data->nama }}"  required="required"  class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>NIM</td>
                        <td> <input type="text" name="nim"  value="{{ $data->nim }}" required="required"  class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td> <input type="text" name="nim"  value="{{ $data->email }}" required="required"  class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>No Telp</td>
                        <td><input type="text" name="no_telp" value="{{$data->no_telp}}" required="required" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Jurusan</td>
                        <td> <input type="text" name="jurusan"  value="{{ $data->jurusan }}" required="required"  class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>Prodi</td>
                        <td> <input type="text" name="prodi_mahasiswa"  value="{{ $data->prodi }}" required="required" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td>Fakultas</td>
                        <td> <input type="text" name="fakultas"  value="{{ $data->fakultas }}" required="required"  class="form-control">
                        </td>
                    </tr>
                </table>
                <input type="submit" value="SAVE" class="save">
            
            </form>
            @endforeach
        </div>
    </div>
</section>






@endsection
